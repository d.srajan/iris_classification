# "Hello World" of Machine learning

A simple example of classifying the flowers as "Iris-setosa", "Iris-versicolor" and "Iris-virginica" based on the length and width of their sepals and petals.

dataset: https://raw.githubusercontent.com/jbrownlee/Datasets/master/iris.csv
https://en.wikipedia.org/wiki/Iris_flower_data_set